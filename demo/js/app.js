var cheap;
$(function(){
  cheap = new Cheaps()
        .setLoading('#loading');

  routie({
    '':function(){
      var data;
      cheap
      .do('fetch data from an api',function(){

        //JQUERY STUFF
        /*$.getJson('url/of/the/api',function(d){
          data = d;
        });*/

        data = {
          'personnes': [
            {'prenom':'dimitri', 'nom':'gigot'},
            {'prenom':'un autre prenom', 'nom':'un autre nom'},
          ]
        }

      })
      .bindTemplate('home-tpl','#main',{},true)   //bind le template 'home-tpl' dans la div '#main'
      .bindTemplate('personnes-row-tpl','#personnes',data,true) //idem en bindant les data avec mustache (true c'est pour dire si qu'on vide #main avant)
      .hideLoading(); //on cache le loading
    },
    '/truc':function(){

      cheap
      .bindTemplate('truc-tpl','#main',{},true)
      .autoBinds([
        ['inputA','#spanA',true],
        ['checkboxA','#spanB',true],
        ['radioA','#spanC',true],
      ])
      .whenIsDo()
      .hideLoading();
    }
  });

});
