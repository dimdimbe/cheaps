/*
**!
***! Cheaps v1.0.1 | (c) 2014
***! Dimitri Gigot - dim@dim-dim.be
**!
*/
if(typeof $=='undefined')console.warn('Jquery is required');
if(typeof Mustache=='undefined')console.warn('Mustache is required');
(function($,window){
  // The `Cheaps` object constructor
  Cheaps = function(){
      //si pas d'instance -> on en crée une
      if (window === this) {
           return new Cheaps();
      }
      //loader #id for show/hide loading animation
      this.loader;
      return this;
  }
  // our dot-operator methods
  Cheaps.prototype = {
      //choose the div#id with the loader animation
      // ( used with showLoading() and hideLoading() )
      setLoading: function(id){
        this.loader = $(id);
        return this;
      },
      //bind a template with data and write it in a destination
      bindTemplate: function(tpl_name,destination,data,clean){
        //get template content
        var templateHtml = $('[data-template-name='+tpl_name+']').html();
        //render data in template with mustache
        var rendedHtml = Mustache.render(templateHtml, data);
        //if clean => true = rewrite all => false = add to content
        if(clean){
          //rewrite all
          $(destination).html(rendedHtml);
        }else{
          //add content
          $(destination).append(rendedHtml);
        }
        return this;
      },
      //bind write a value in a destination
      bind: function(value,destination,clean){
        //if clean => true = rewrite all => false = add to content
        if(clean){
          //rewrite all
          $(destination).html(value);
        }else{
          //add content
          $(destination).append(value);
        }
        return this;
      },
      //bind write a value in an Input destination
      bindInput: function(value,destination){

        $(destination).val(value);

        return this;
      },
      //do what you want
      do:function(name,callback){
        callback();
        return this;
      },
      //listen an input and bind data in an elem with id
      autoBind: function(input_name,destination,clean){
        //dont loose the scope :)
        _this = this;

        var input = $('[name="'+input_name+'"]');
        var type = input.attr('type');

        if(input[0].tagName =='SELECT'){
          type='select';
        }

        if(type=='text'){
            _this.bind(input.val(),destination,clean);
            input.keyup(function() {
                _this.bind(input.val(),destination,clean);
            });
        }else if(type=='radio'){
            var initval = $('[name="'+input_name+'"]:checked').val();
            _this.bind(initval,destination,clean);

            input.change(function(){
                var val = $('[name="'+input_name+'"]:checked').val();
                _this.bind(val,destination,clean);
            });

        }else if(type=='checkbox'){
            var elems = $('[name="'+input_name+'"]:checked');
            var initval = '';
            for(var i = 0; i<elems.length; i++ ){
                var elem = elems[i];
                initval += elem.value;
                if(i!=elems.length-1){
                  initval +=' + ';
                }
            }
            _this.bind(initval,destination,clean);

            input.change(function(){
                var elems = $('[name="'+input_name+'"]:checked');
                var val='';
                for(var i = 0; i<elems.length; i++ ){
                    var elem = elems[i];
                    val += elem.value;
                    if(i!=elems.length-1){
                      val +=' + ';
                    }
                }
                _this.bind(val,destination,clean);
            });
        }else if(type=='select'){
          var val='';
          for(var i=0; i<input.length;i++){
              val = input[i].value;
          }
          _this.bind(val,destination,clean);

          input.change(function(){
              var val='';
              for(var i=0; i<input.length;i++){
                val = input[i].value;
              }
              _this.bind(val,destination,clean);
          });

        }else{
          console.warn('not handle input type: "'+type+'"  yet!');
        }

        return this;
      },
      //listen an array of inputs and autoBind them
      autoBinds: function(elems){
        for(var i = 0; i<elems.length ; i++){
          var elem = elems[i];

          this.autoBind(elem[0],elem[1],elem[2]);

        }
        return this;
      },
      //show loading elem (setted by setLoading method)
      showLoading: function(){
        this.loader.show();
        return this;
      },
      //hide loading elem
      hideLoading: function(){
        this.loader.hide();
        return this;
      },
      //listen change of $(selector)
      //do vrai() of faux()
      //when $(selector).is(condition)
      whenIsDo:function(name,selector,condition,vrai,faux){

        $(selector).change(function(){
            cond = $(this).is(condition);
            if(cond){
              vrai($(this));
            }else{
              faux($(this));
            }

        });

        return this;
      },
      //redirect to path
      redirect: function(path){
          window.location.replace('#'+path);
          return this;
      },
      //add or remove last '/' from the url (make a refresh of the current page)
      refresh: function(){
        var path = window.location.hash.substr(1);
        if(path[path.length-1]!='/'){
          this.redirect(path+'/');
        }else{
          path= path.substr(0,path.length-1);
          this.redirect(''+path);
        }
        return this;
      },
      //change page title
      setTitle: function(title){
        document.title=title;
        return this;
      },
      //log the cheap object in console
      log:function(){
        //console -> Cheaps object
        console.log(this);
        return this;
      },
      //todo method
      todo:function(title,task){
        console.warn('TODO : '+title);
        for(var i in task){
          console.log(' |-> '+task[i]);
        }
        console.log(' ');
        return this;
      },
  };
})($,window);
